package edu.wm.cs.cs301.slidingpuzzle;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Random;

public class SimplePuzzleState implements PuzzleState {
	
	int pathLength = 0; //info that every puzzle state needs to have
	Operation prevOperation = null;
	PuzzleState parent = null;
	int[][] board; 
	
	@Override
	public void setToInitialState(int dimension, int numberOfEmptySlots) {
		 board = new int[dimension][dimension];
		 int count = 1;
		 for(int i = 0; i < dimension; i++){
			 for(int j = 0; j < dimension; j++){
				 if(count >= dimension*dimension - numberOfEmptySlots + 1){ //scan 2d array and place increasing values stopping when empty slots is reached
					 board[i][j] = 0;
				 }
				 else{
					 board[i][j] = count;
					 count += 1;
				 }
			 }
		 }
	}

	@Override
	public int getValue(int row, int column) {		//return the desired value 
		return board[row][column]; 
	}

	
	@Override
	public PuzzleState getParent() {
			return parent;
	}

	@Override
	public Operation getOperation() {
		return prevOperation;
	}

	@Override
	public int getPathLength() {
		return pathLength;
	}

	@Override
	public SimplePuzzleState move(int row, int column, Operation op) {
		if(this.checkValidMove(row, column, op)){
			SimplePuzzleState newState = new SimplePuzzleState();
			newState.board = this.returnBoard();
			newState.moveValue(op, row, column);
			newState.parent = this;
			newState.pathLength = 1 + this.pathLength;
			newState.prevOperation = op;
			return newState;
		}
		else{
			return null;
		}
	}

	@Override
	public SimplePuzzleState drag(int startRow, int startColumn, int endRow, int endColumn) {
		Random rand = new Random();
		ArrayList<Operation> moves = new ArrayList<Operation>();
		if((startRow == endRow)&&(startColumn == endColumn))
			return this;
		
		if(!(this.isEmpty(endRow, endColumn)))
				return null;
		else{
			int rowDif = startRow - endRow, colDif = startColumn - endColumn;
			if(rowDif < 0){ // if rows need to be moved down during the operation
				for(;rowDif < 0; rowDif++)
					moves.add(Operation.MOVEDOWN);
			}				
			if(rowDif > 0){ // if rows need to be moved down during the operation
				for(;rowDif > 0; rowDif--)
					moves.add(Operation.MOVEUP);
			}		
			if(colDif < 0){ // if rows need to be moved down during the operation
				for(;colDif < 0; colDif++)
					moves.add(Operation.MOVERIGHT);
			}				
			if(colDif > 0){ // if rows need to be moved down during the operation
				for(;colDif > 0; colDif--)
					moves.add(Operation.MOVELEFT);
			}
			
			while(!(moves.isEmpty())){
				SimplePuzzleState nextState;
				Operation operation = moves.get(rand.nextInt(moves.size()));
				nextState = this.move(startRow, startColumn, operation);
				if(nextState != null) //ensure there are no null pointer errors from faulty moves
					switch(operation){
					case MOVEUP:
						return nextState.drag(startRow - 1, startColumn, endRow, endColumn);
					case MOVEDOWN:
						return nextState.drag(startRow + 1, startColumn, endRow, endColumn);
					case MOVELEFT:
						return nextState.drag(startRow, startColumn - 1, endRow, endColumn);
					case MOVERIGHT:
						return nextState.drag(startRow, startColumn + 1, endRow, endColumn);
					}
			}
			return null;
		}
	}

	@Override
	public SimplePuzzleState shuffleBoard(int pathLength) {
		if(pathLength==0){
			return this;
		}
		Random rand = new Random();
		int moveRow = 0, moveCol = 0;
		ArrayList<Operation> possibleMoves = new ArrayList<Operation>();
		SimplePuzzleState nextState = new SimplePuzzleState();
		for(int row=0; (row < this.board.length); row++){
			for(int column=0; column < this.board.length; column++){
				if(this.isEmpty(row, column)){
					try{
						if(!(this.isEmpty(row +1, column))){
							possibleMoves.add(Operation.MOVEUP);
						}
					}
					catch(Exception e){
						; //do nothing if the attempt was out of bounds
					}
					try{
						if(!(this.isEmpty(row -1, column))){
							possibleMoves.add(Operation.MOVEDOWN);
						}
					}
					catch(Exception e){
						;
					}
					try{
						if(!(this.isEmpty(row, column - 1))){
							possibleMoves.add(Operation.MOVERIGHT);
						}
					}
					catch(Exception e){
						;
					}
					try{
						if(!(this.isEmpty(row, column + 1))){
							possibleMoves.add(Operation.MOVELEFT);
						}
					}
					catch(Exception e){
						;
					}
					int direction = rand.nextInt(possibleMoves.size());
					Operation operator = possibleMoves.get(direction);
					boolean moveSelected = false;
					while(!(moveSelected)){ //while no move has been selected
						direction = rand.nextInt(possibleMoves.size()); //chose one of the available moves at random
						operator = possibleMoves.get(direction);
						switch(operator){
							case MOVEUP:
								if(this.getOperation()== Operation.MOVEDOWN)
									break;
								moveRow = ++row;
								moveCol = column;
								moveSelected = true;
								break;
							case MOVEDOWN:
								if(this.getOperation()== Operation.MOVEUP)
									break;								
								moveRow = --row;
								moveCol = column;
								moveSelected = true;
								break;
							case MOVELEFT:
								if(this.getOperation()== Operation.MOVERIGHT)
									break;
								moveRow = row;
								moveCol = ++column;
								moveSelected = true;
								break;
							case MOVERIGHT:
								if(this.getOperation()== Operation.MOVELEFT)
									break;
								moveRow = row;
								moveCol = --column;
								moveSelected = true;
								break;
						}
					}
					nextState = this.move(moveRow, moveCol, operator);
					return nextState.shuffleBoard(pathLength-1);
				}
				else{
					continue; //if the cell not empty just keep looking
				}
			}
		}
		return null;
	}

	@Override
	public boolean isEmpty(int row, int column) {
		if(board[row][column] == 0){
			return true;
		}
		else{
			return false;
		}
	}

	@Override
	public SimplePuzzleState getStateWithShortestPath() {
		return this; //what is this supposed to do>>
	}
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj){ //if it is self, then true
			return true;
		}
		if(!(obj instanceof SimplePuzzleState)){ //if it is not even a comparable object obv false
			return false;
		}
		SimplePuzzleState compare = (SimplePuzzleState) obj;
		if(this.board == null){ //with the obj now typecast we can compare arrays
			return (compare.board == null);
		}
		if(compare.board == null){
			return false;
		}
		for(int i = 0; i < this.board.length; i++){
			if(!Arrays.equals(this.board[i], compare.board[i])){
			return false;
			}
		}
		return true;
	}
		
	@Override
	public int hashCode(){
		return 1;
	}
	
	
	private int[][] returnBoard(){
		
		int dimension = this.board.length;
		int[][] clonedBoard = new int[dimension][dimension];
		for(int i=0; i < dimension; i++) {
			for(int j=0; j < dimension; j++){
				clonedBoard[i][j] = this.board[i][j];
			}
		}
		return clonedBoard;
	}
	
	private boolean checkValidMove(int row, int column, Operation op){
		if((op == Operation.MOVERIGHT)&&this.isEmpty(row, column + 1)){
			return true;
		}
		else if((op == Operation.MOVELEFT)&&this.isEmpty(row, column - 1)){
			return true;
		}
		else if((op == Operation.MOVEDOWN)&&this.isEmpty(row + 1, column)){
			return true;
		}
		else if((op == Operation.MOVEUP)&&this.isEmpty(row - 1, column)){
			return true;
		}
		else{
			return false;
		}
	}
	
	private void moveValue(Operation op, int row, int col){ //moves value in given direction on internal game board structure
		if(op == Operation.MOVERIGHT){
			this.board[row][col + 1] = this.board[row][col];
			this.board[row][col] = 0;
		}
		else if(op == Operation.MOVELEFT){
			this.board[row][col - 1] = this.board[row][col];
			this.board[row][col] = 0;
		}
		else if(op == Operation.MOVEDOWN){
			this.board[row + 1][col] = this.board[row][col];
			this.board[row][col] = 0;
		}
		else if(op == Operation.MOVEUP){
			this.board[row - 1][col] = this.board[row][col];
			this.board[row][col] = 0;
		}
	}
}
